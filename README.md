# CarCar

Team:

* Zara, Jonas - Services
* Sanford, Christopher - Sales

## Design

## Service microservice

The Service microservice will use a polling script to retrieve Automobile vin and sold data from the Inventory API. This data will be mirrored to the AutomobileVO model created within the Service microservice. This way, the Service and Inventory microservices can be separate yet connected within the project allowing the Service microservice to focus on services, and the inventory microservice to focus on inventories.

## Sales microservice

Here at sales, our microservice keeps track of customers, sales people, and sales. Sales interact with the inventory microservice in order to know what cars we have available to sell. It also keeps track of who sold which car to which customer. Much like the service microservice, sales is standalone, keeping track of sales and customers while inventory can focus on, well inventory.




## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone <<respository.url.here>>

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/

## Design

CarCar is made up of 3 microservices which interact with one another.

- **Inventory**
- **Services**
- **Sales**

![CarCar Diagram](<CarCar JC.png>)


## Integration - How we put the "team" in "team"

Our Inventory and Sales domains work together with our Service domain to make everything here at **CarCar** possible.

How this all starts is at our inventory domain. We keep a record of automobiles on our lot that are available to buy. Our sales and service microservices obtain information from the inventory domain, using a **poller**, which talks to the inventory domain to keep track of which vehicles we have in our inventory so that the service and sales team always has up-to-date information.


## Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser

### Manufacturers:


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/


JSON body to send data:

Create and Update a manufacturer (SEND THIS JSON BODY):
- You cannot make two manufacturers with the same name
```
{
  "name": "Chrysler"
}
```
The return value of creating, viewing, updating a single manufacturer:
```
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Chrysler"
}
```
Getting a list of manufacturers return value:
```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}
```

### Vehicle Models:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

Create and update a vehicle model (SEND THIS JSON BODY):
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
  "manufacturer_id": 1
}
```

Updating a vehicle model can take the name and/or picture URL:
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
}
```
Return value of creating or updating a vehicle model:
- This returns the manufacturer's information as well
```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```
Getting a List of Vehicle Models Return Value:
```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "image.yourpictureurl.com",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```

### Automobiles:
- The **'vin'** at the end of the detail urls represents the VIN for the specific automobile you want to access. This is not an integer ID. This is a string value so you can use numbers and/or letters.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/


Create an automobile (SEND THIS JSON BODY):
- You cannot make two automobiles with the same vin
```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```
Return Value of Creating an Automobile:
```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "777",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "R8",
		"picture_url": "image.yourpictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	}
}
```
To get the details of a specific automobile, you can query by its VIN:
example url: http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/

Return Value:
```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "green",
  "year": 2011,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "image.yourpictureurl.com",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  }
}
```
You can update the color and/or year of an automobile (SEND THIS JSON BODY):
```
{
  "color": "red",
  "year": 2012
}
```
Getting a list of Automobile Return Value:
```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "image.yourpictureurl.com",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }
  ]
}
```
# Sales Microservice

On the backend, the sales microservice has 4 models: AutomobileVO, Customer, SalesPerson, and SalesRecord. SalesRecord is the model that interacts with the other three models. This model gets data from the three other models.

The AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The sales poller automotically polls the inventory microservice for data, so the sales microservice is constantly getting the updated data.

The reason for integration between these two microservices is that when recording a new sale, you'll need to choose which car is being sold and that information lives inside of the inventory microservice.


## Accessing Endpoints to Send and View Data - Access through Insomnia:

### Customers:


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Delete a specific customer | DELETE | http://localhost:8090/api/customers/id/

To create a Customer (SEND THIS JSON BODY):
```
{
	"first_name":"Gerald",
	"last_name":"Game",
	"address":"555 Main",
	"phone_number":"8675309"
}
```
Return Value of Creating a Customer:
```
{
	"first_name": "Gerald",
	"last_name": "Game",
	"address": "555 Main",
	"phone_number": "8675309",
	"id": 2
}
```
Return value of Listing all Customers:
```
{
	"customer": [
		{
			"first_name": "Billy",
			"last_name": "Bob",
			"address": "123 sadlfgkj",
			"phone_number": "1234",
			"id": 1
		},
		{
			"first_name": "Gerald",
			"last_name": "Game",
			"address": "555 Main",
			"phone_number": "8675309",
			"id": 2
		}
	]
}
```
### Salespeople:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Salesperson details | GET | http://localhost:8090/api/salesperson/id/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/id/


To create a salesperson (SEND THIS JSON BODY):
```
{
	"first_name":"Billy",
	"last_name":"Bob",
	"employee_id":"123"
}
```
Return Value of creating a salesperson:
```
{
	"first_name": "Billy",
	"last_name": "Bob",
	"employee_id": "123",
	"id": 2
}
```
List all salespeople Return Value:
```
{
	"salespeople": [
		{
			"first_name": "AGSDF",
			"last_name": "WERYT",
			"employee_id": "324",
			"id": 1
		},
		{
			"first_name": "Billy",
			"last_name": "Bob",
			"employee_id": "123",
			"id": 2
		}
	]
}
```
### Salesrecords:
- the id value to show a salesperson's salesrecord is the **"id" value tied to a salesperson.**

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all salesrecords | GET | http://localhost:8090/api/sales/
| Create a new sale | POST | http://localhost:8090/api/sales/

List all Salesrecords Return Value:
```
{
	"sales": [
		{
			"automobile": "123",
			"sales_person": "AGSDF",
			"customer": "Bob",
			"price": 45,
			"id": 1
		},
		{
			"automobile": "123",
			"sales_person": "AGSDF",
			"customer": "Bob",
			"price": 1,
			"id": 2
		}
	]
}
```
Create a New Sale (SEND THIS JSON BODY):
```
{
	"automobile":"1C3CC5FB2AN120174",
	"sales_person":"1",
	"customer":"1",
	"price":"3452"
}
```
Return Value of Creating a New Sale:
```
{
	"automobile": "1C3CC5FB2AN120174",
	"sales_person": "AGSDF",
	"customer": "Bob",
	"price": "3452",
	"id": 3
}
```



# Service microservice

Hello and welcome to the wonderful world of service!!
As explained above, the service microservice is an extension of the dealership that looks to provide service repairs for your vehicle.

As automobiles are purchased, we keep track of the vin number of each automobile and you are able to receive the special perks of being a VIP if you've bought from our inventory.
As a VIP, you will receive free oil changes for life, complimentary neck massages while in our waiting room, and a 1.5 year Muay Thai training session in Thailand.

This area is going to be broken down into the various API endpoints (Fancy way of saying your web address url) for service along with the format needed to send data to each component.
The basics of service are as follows:
1. Our friendly technician staff
2. Service Appointments


### Technicians - The heart of what we do here at CarCar
(We are considering renaming, don't worry)

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Create a technician | POST | http://localhost:8080/api/technicians/


LIST TECHNICIANS: Following this endpoint will give you a list of all technicians that are currently employed.
Since this is a GET request, you do not need to provide any data.

CREATE TECHNICIAN - What if we hired a new technician (In this economy even)? To create a technician, you would use the following format to input the data and you would just submit this as a POST request.
```
{
	"first_name": "Liz",
    "last_name": "Fan",
	"employee_id": FanFan
}
```


### Service Appointments: We'll keep you on the road and out of our waiting room

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List service appointments | GET | http://localhost:8080/api/appointments/
| Create service appointment | POST | http://localhost:8080/api/appointments/
| Appointment status to 'finished' | PUT | http://localhost:8080/api/appointments/id/finish/
| Appointment status to 'canceled' | PUT | http://localhost:8080/api/appointments/id/cancel/


LIST APPOINTMENTS: Following this endpoint will give you a list of all appointments. Since this is a GET request, you do not need to provide any data.


CREATE SERVICE APPOINTMENT - This will create a service appointment with the data input. It must follow the format. Here is an example below.
```
		{
			"date_time": "2024-02-13T13:30",
			"reason": "Oil Change",
			"vin": "FSDLFJ39393244442",
			"customer": "Patrick",
			"technician": 1
		}

```
UPDATE APPOINTMENT STATUS - These endpoints require no JSON bodies as data, such is the nature of PUT requests thanks to its corresponding view function. Simply change the id within the link provided to the desired id of the appointment you wish to either finish or cancel. Once this is done, send the request through Insomnia and the response should show a message saying your appointment has been either finished or canceled successfully. You can double check this through the list appointment get request.


### AutomomobileVO: Our precious inventory

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List AutomobileVOs | GET | http://localhost:8080/api/automobiles


LIST AutomobileVOs: Following this endpoint will give you a list of all automobiles being polled from the Inventory Microservice. Since this is a GET request, you do not need to provide any data.
