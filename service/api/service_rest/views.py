from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO

# Create your views here.

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        'first_name',
        'last_name',
        'employee_id',
        'id',
        ]

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):

    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    elif request.method == "POST":
        data = json.loads(request.body)
        try:
            technician = Technician.objects.create(
                first_name = data['first_name'],
                last_name = data['last_name'],
                employee_id = data['employee_id']
            )
            return JsonResponse(technician, encoder=TechnicianListEncoder, safe=False)
        except (KeyError, TypeError, ValueError) as e:
            return JsonResponse((f"An error occurred: {e}"), status=400)


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'date_time',
        'reason',
        'status',
        'vin',
        'customer',
        'id',
        ]
    def get_extra_data(self, appointment):
        if appointment.technician is not None:
            return {
                'technician':{
                    'first_name': appointment.technician.first_name,
                    'last_name': appointment.technician.last_name,
                    'employee_id': appointment.technician.employee_id,
                    }
            }
        return None

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):

    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )
    elif request.method == "POST":
        data = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=data['technician'])
            data['technician'] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid technician id"}, status=400)
        appointment = Appointment.objects.create(**data)
        return JsonResponse(appointment, encoder=AppointmentListEncoder, safe=False)


@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = 'canceled'
        appointment.save()
        return JsonResponse({"message": "Appointment canceled successfully"})
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Invalid appointment id"}, status=400)


@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = 'finished'
        appointment.save()
        return JsonResponse({"message": "Appointment finished successfully"})
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Invalid appointment id"}, status=400)


class AutomobileVOListEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'vin',
        'sold',
        ]

@require_http_methods(["GET"])
def api_get_automobiles(request):
    if request.method == 'GET':
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOListEncoder,
            safe=False
            )
