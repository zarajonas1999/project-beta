from django.urls import path

# from .views import pass
from .views import api_list_technicians, api_list_appointments, api_cancel_appointment, api_finish_appointment, api_get_automobiles

urlpatterns = [
    path('technicians/', api_list_technicians, name='api_list_technicians'),
    path('appointments/', api_list_appointments, name='api_list_appointments'),
    path('appointments/<int:id>/cancel/', api_cancel_appointment, name='api_cancel_appointment'),
    path('appointments/<int:id>/finish/', api_finish_appointment, name='api_finish_appointment'),
    path('automobiles/', api_get_automobiles, name='api_get_automobiles'),
]
