from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

STATUS_CHOICES = (
    ('created', 'Created'),
    ('canceled', 'Canceled'),
    ('finished', 'Finished'),
)
class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=9, choices=STATUS_CHOICES, default='created')
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=50)
    technician = models.ForeignKey(
        Technician,
        on_delete = models.CASCADE,
        related_name = 'appointments'
    )

    def __str__(self):
        return self.customer
