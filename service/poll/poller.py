import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here. Ignore vs-code error hinting
# from service_rest.models import Something
from service_rest.models import AutomobileVO


def poll():
    while True:
        print('Service poller polling for data')
        try:
            response = requests.get('http://inventory-api:8000/api/automobiles/')
            if response.status_code == 200:
                automobiles = response.json()['autos']
                for automobile in automobiles:
                    AutomobileVO.objects.update_or_create(
                        vin=automobile['vin'],
                        defaults={
                            'sold': automobile['sold'],
                        }
                    )
                print('Poll is a go *thumbs up')
            else:
                print(f"Failed to fetch automobiles: Status code {response.status_code}")

        except Exception as e:
            print(f"An error occurred: {e}", file=sys.stderr)
        time.sleep(60)  # Poll every 60 seconds

if __name__ == "__main__":
    poll()
