import React, { useState, useEffect } from 'react';

function ListAppointmentsHistory() {

    const [appointments, setAppointments] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);
    const fetchData = async () => {
        const appointmentsURL = 'http://localhost:8080/api/appointments/';
        const automobilesURL = 'http://localhost:8080/api/automobiles/'; //literally missed the last forward slash here and the resulting CORS error nearly gave me Marie Antoinette syndrome
        const appointmentsResponse = await fetch(appointmentsURL);
        const automobilesResponse = await fetch(automobilesURL);
        if (appointmentsResponse.ok && automobilesResponse.ok) {
          const appointmentsData = await appointmentsResponse.json();
          setAppointments(appointmentsData.appointments);

          const automobilesData = await automobilesResponse.json();
          setAutomobiles(automobilesData.automobiles);
        } else{console.error('Fetch failed')};
      };

    useEffect(() => {
        fetchData();
    }, []);


    const[filteredAppointments, setFilteredAppointments] = useState([]);

    const[searchedVIN,setSearchedVIN] = useState('');
    const handleSearchedVINChange = (event) => {
        const value = event.target.value;
        setSearchedVIN(value)
    }


    const isVIP = (vin) => {
        return automobiles.some(automobile => automobile.vin === vin);
    };


    const handleSearchVIN = (event) => {
        event.preventDefault();
        const inputVIN = appointments.filter(appointment => appointment.vin === searchedVIN);
        setFilteredAppointments(inputVIN);
    }

    return (
        <div>
            <form onSubmit={handleSearchVIN}>
                <input
                    type="text"
                    maxLength='17'
                    value={searchedVIN}
                    onChange={handleSearchedVINChange}
                />
                <button type='submit'>Search by VIN</button>
            </form>
            <table className='table'>
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP</th>
                        <th>Customer</th>
                        <th>Date/Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredAppointments.length > 0 ? filteredAppointments.map(appointment => (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{isVIP(appointment.vin) ? 'Yes' : 'No'}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.date_time}</td>
                            <td>{appointment.technician.employee_id}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                        </tr>
                    )) : appointments.map(appointment => (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{isVIP(appointment.vin) ? 'Yes' : 'No'}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.date_time}</td>
                            <td>{appointment.technician.employee_id}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ListAppointmentsHistory;
