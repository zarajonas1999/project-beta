import React, { useState, useEffect } from 'react';

function ListAppointmentsHistory() {

    const [appointments, setAppointments] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);
    const fetchData = async () => {
        const appointmentsURL = 'http://localhost:8080/api/appointments/';
        const automobilesURL = 'http://localhost:8080/api/automobiles/'; //literally missed the last forward slash here and the resulting CORS error nearly gave me Marie Antoinette syndrome from all the docs I had to read
        const appointmentsResponse = await fetch(appointmentsURL);
        const automobilesResponse = await fetch(automobilesURL);
        if (appointmentsResponse.ok && automobilesResponse.ok) {
          const appointmentsData = await appointmentsResponse.json();
          setAppointments(appointmentsData.appointments);

          const automobilesData = await automobilesResponse.json();
          setAutomobiles(automobilesData.automobiles);
        } else{console.error('Fetch failed')};
      };

    useEffect(() => {
        fetchData();
    }, []);


    const isVIP = (vin) => {
        return automobiles.some(automobile => automobile.vin === vin);
    };


    const handleCancelStatus = async (id) => {
        const cancelURL = `http://localhost:8080/api/appointments/${id}/cancel/`;
        const response = await fetch(cancelURL, {
            method:'PUT',
        });

        if (response.ok) {
            const updatedAppointments = appointments.map((appointment) => {
                if(appointment.id === id){
                    return {...appointment, status:'canceled'};
                }
                else {
                    return appointment;
                }
            });
            setAppointments(updatedAppointments);
            }
    }


    const handleFinishStatus = async (id) => {
        const finishURL = `http://localhost:8080/api/appointments/${id}/finish/`;
        const response = await fetch(finishURL, {
            method:'PUT',
        });

        if (response.ok) {
            const updatedAppointments = appointments.map((appointment) => {
                if(appointment.id === id){
                    return {...appointment, status:'finished'};
                }
                else {
                    return appointment;
                }
            });
            setAppointments(updatedAppointments);
            }
    }

    return (
        <table className='table'>
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>VIP</th>
                    <th>Customer</th>
                    <th>Date/Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {appointments.filter(appointment => appointment.status === 'created').map(appointment => (
                    <tr key={appointment.id}>
                        <td>{appointment.vin}</td>
                        <td>{isVIP(appointment.vin) ? 'Yes' : 'No'}</td>
                        <td>{appointment.customer}</td>
                        <td>{appointment.date_time}</td>
                        <td>{appointment.technician.employee_id}</td>
                        <td>{appointment.reason}</td>
                        <td>
                            <button onClick = {() => handleCancelStatus(appointment.id)}>cancel</button>
                            <button onClick = {() => handleFinishStatus(appointment.id)}>finish</button>
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default ListAppointmentsHistory;
