import React, {useEffect, useState} from 'react';


function CreateModels () {

    const [modelName,setModelName] = useState('');
    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    };

    const[pictureURL,setPictureURL] = useState('');
    const handlePictureURLChange = (event) => {
        const value = event.target.value;
        setPictureURL(value);
    };

    const[manufacturer,setManufacturerName] = useState('');
    const handleManufacturerNameChange = (event) => {
        const value = event.target.value;
        setManufacturerName(value);
    };

    const [manufacturers, setManufacturers] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setManufacturers(data.manufacturers);
        };
      };
    useEffect(() => {fetchData();}, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = modelName;
        data.picture_url = pictureURL;
        data.manufacturer_id = manufacturer;

        const modelsURL = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',},
        };
        const response = await fetch(modelsURL, fetchConfig);
        if (response.ok){
            setModelName('');
            setPictureURL('');
            setManufacturerName('');
            event.target.reset();
        };
    };

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new model</h1>
            <form
            id="create-technician-form"
            onSubmit = {handleSubmit}
            >
              <div className="form-floating mb-3">
                <input
                onChange={handleModelNameChange}
                value={modelName}
                placeholder="Name"
                required type="text"
                name="name"
                id="name"
                className="form-control"
                />
                <label htmlFor="name">Model name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handlePictureURLChange}
                value={pictureURL}
                placeholder="Picture URL"
                required type="url"
                name="picture"
                id="picture"
                className="form-control"
                />
                <label htmlFor="picture">Picture URL</label>
              </div>
              <div className="mb-3">
                <select
                onChange={handleManufacturerNameChange}
                value={manufacturer}
                required name="manufacturer name"
                id="manufacturer name"
                className="form-select"
                >
                  <option  value="">Choose a manufacturer</option>
                  {manufacturers.map(manufacturer => {
                    return (
                        <option key = {manufacturer['id']} value = {manufacturer['id']}>
                            {manufacturer['name']}
                        </option>
                    )
                  }) }
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}






export default CreateModels;
