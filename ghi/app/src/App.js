import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import CreateTechnicans from './CreateTechnicians'
import ListTechnicans from './ListTechnicians'
import SalesPeopleList from './SalesPeopleList';
import SalesPersonForm from './SalespersonForm';
import NewSalesForm from './SaleRecordForm';
import SalesList from './SaleRecordList';
import CustomerForm from './CreateCustomerForm';
import CustomerList from './CustomerList';
import SalesPersonHistory from './SaleRecordHistory';import CreateAppointment from './CreateAppointment';
import ListAppointments from './ListAppointments';
import ListAppointmentsHistory from './ListAppointmentsHistory';
import CreateManufacturers from './CreateManufacturers';
import ListManufacturers from './ListManufacturers';
import ListModels from './ListModels';
import CreateModels from './CreateModels';
import ListAutomobiles from './ListAutomobiles';
import CreateAutomobiles from './CreateAutomobile';


function App() {


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians/create" element={<CreateTechnicans />} />
          <Route path="/technicians/" element={<ListTechnicans />} />
          <Route path="/salespeople/create" element={<SalesPersonForm />} />
          <Route path="/salespeople/" element={<SalesPeopleList />} />
          <Route path="/customers/create" element={<CustomerForm />} />
          <Route path="/customers/" element={<CustomerList />} />
          <Route path="/sales/create" element={<NewSalesForm />} />
          <Route path="/sales/" element={<SalesList />} />
          <Route path="/salespersonhistory/" element={<SalesPersonHistory />} />
          <Route path="/appointments/create" element={<CreateAppointment />} />
          <Route path="/appointments/" element={<ListAppointments />} />
          <Route path="/appointments/history" element={<ListAppointmentsHistory />} />
          <Route path="/manufacturers/create" element={<CreateManufacturers />} />
          <Route path="/manufacturers" element={<ListManufacturers />} />
          <Route path="/models" element={<ListModels />} />
          <Route path="/models/create" element={<CreateModels />} />
          <Route path="/automobiles" element={<ListAutomobiles />} />
          <Route path="/automobiles/create" element={<CreateAutomobiles />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
