import React, { useState, useEffect } from 'react';

function ListManufacturers() {

    const [manufacturerName, setManufacturer] = useState([]);
    const fetchData = async () => {
        const getURL = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(getURL);
        if (response.ok) {
          const data = await response.json();
          setManufacturer(data.manufacturers);
        };
      };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className='table'>
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                {manufacturerName.map(manufacturer => (
                    <tr key={manufacturer.id}>
                        <td>{manufacturer.name}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default ListManufacturers;
